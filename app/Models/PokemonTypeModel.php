<?php

namespace App\Models;

use CodeIgniter\Model;


class PokemonTypeModel extends Model
{
    protected $table = 'pokemon__pokemon_type';
    protected $primaryKey = 'id_pokemon';

    protected $allowedFields = [
        'id_pokemon',
        'id_pokemon_type',
    ];

    public function updateType($id, $idType, $idOldType)
    {
        $type_1 = "type1";
        $type_2 = "type2";


        $builder = $this->db->table('pokemon__pokemon_type');
        $builder->set(['id_pokemon_type' => $idType]);
        $builder->where(['id_pokemon' => $id]);
        $builder->update();
        $result = $builder->get()->getResult();
        return json_decode(json_encode($result), true);
    }
}
