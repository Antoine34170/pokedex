<?php

namespace App\Models;

use CodeIgniter\Model;


class PokemonModel extends Model
{
    protected $table = 'pokemon';
    protected $primaryKey = 'id_pokemon';


    protected $allowedFields = [
        'numero',
        'name',
        'hp',
        'attack',
        'defense',
        'speed',
        'special',
        'picture',
        'slug',
        'type_1',
        'type_2',
    ];

    public function getPokemons($slug = false)
    {

        if ($slug === false) {
            return $this->findAll();
        }

        return $this->asArray()
            ->where(['slug' => $slug])
            ->first();
    }

    public function getPokemonTypes($slug = false)
    {

        if ($slug === false) {
            $builder = $this->db->table('pokemon_type');
            $builder->select('name_type');
            $result = $builder->get()->getResult();
            return json_decode(json_encode($result), true);
        }

        $builder = $this->db->table('pokemon_type');
        $builder->select('name_type');
        $builder->join('pokemon__pokemon_type', 'pokemon__pokemon_type.id_pokemon_type = pokemon_type.id');
        $builder->join('pokemon', 'pokemon.id = pokemon__pokemon_type.id_pokemon');
        $builder->where(['pokemon.slug' => $slug]);
        $result = $builder->get()->getResult();
        return json_decode(json_encode($result), true);
    }

    public function removePokemon($slug)
    {
        return $this->delete(['slug' => $slug]);
    }
}
