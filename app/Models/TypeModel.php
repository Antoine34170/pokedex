<?php

namespace App\Models;

use CodeIgniter\Model;


class TypeModel extends Model
{
    protected $table = 'pokemon_type';
    protected $primaryKey = 'id_type';

    protected $allowedFields = [
        'id_type',
        'name_type'
    ];


    public function getTypeOneName($slug)
    {
        return $this->select('name_type, id_type')->join('pokemon', 'pokemon.type_1 = pokemon_type.id_type')->where(['slug' => $slug])->findAll();
    }

    public function getTypeTwoName($slug)
    {
        return $this->select('name_type, id_type')->join('pokemon', 'pokemon.type_2 = pokemon_type.id_type')->where(['slug' => $slug])->findAll();
    }

    public function getTypes()
    {
        return $this->findAll();
    }

    public function getTypeColor($idType)
    {
        return $this->select('pokemon.id_pokemon,pokemon.name,pokemon.numero, name_type,color_type')->join('pokemon', 'pokemon.type_1 = pokemon_type.id_type')->where(['id_type' => $idType])->findAll();
    }

    public function getTypeOneColor($slug)
    {
        return $this->select('pokemon.id_pokemon,pokemon.name,pokemon.numero, name_type,color_type')->join('pokemon', 'pokemon.type_1 = pokemon_type.id_type')->where(['slug' => $slug])->findAll();
    }

    public function getTypeTwoColor($slug)
    {
        return $this->select('pokemon.id_pokemon,pokemon.name,pokemon.numero, name_type,color_type')->join('pokemon', 'pokemon.type_2 = pokemon_type.id_type')->where(['slug' => $slug])->findAll();
    }
}
