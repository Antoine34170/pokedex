<?php

namespace App\Controllers;

use App\Models\PokemonModel;
use App\Models\PokemonTypeModel;
use App\Models\TypeModel;
use CodeIgniter\Controller;


class Pokemon extends Controller
{

	public function index()
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();
		$data = [
			'pokemon'  => $pokemonModel->getPokemons(),
			'title' => 'Pokedex',
			'types' => $typeModel->getTypes(),
		];
		echo view('templates/header', $data);
		echo view('pokemon/overview', $data);
		echo view('templates/footer', $data);
	}

	public function view($slug)
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();
		$data = [
			'pokemon' => $pokemonModel->getPokemons($slug),
			'typeOne' => $typeModel->getTypeOneName($slug),
			'typeTwo' => $typeModel->getTypeTwoName($slug),
			// 'typeColor' => $typeModel->getTypeColor($id_type)
		];
		if (empty($data['pokemon'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the pokemon : ' . $slug);
		}
		echo view('templates/header');
		echo view('pokemon/view', $data);
		echo view('templates/footer');
	}

	public function create()
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();

		$data = [
			'types' => $typeModel->getTypes(),
		];

		if ($this->request->getMethod() === 'post' && $this->validate([
			'numero' => 'required|min_length[1]|max_length[3]',
			'name' => 'required|min_length[3]|max_length[255]',
			'hp' => 'required',
			'attack' => 'required',
			'defense' => 'required',
			'speed' => 'required',
			'special' => 'required',
			'type_1' => 'required',
			'type_2' => 'required',
		])) {
			$type_1 = $this->request->getPost('type_1');
			$type_2 = $this->request->getPost('type_2');
			if ($type_1 == $type_2) {
				// Which is 'no type found' to avoid dual type and BDD error
				$type_2 = 20;
			} else if ($type_1 == $type_2 && $type_1 == 20) {
				$type_2 = null;
			}

			$pokemonModel->save([
				'numero' => $this->request->getPost('numero'),
				'name' => $this->request->getPost('name'),
				'hp' => $this->request->getPost('hp'),
				'attack' => $this->request->getPost('attack'),
				'defense' => $this->request->getPost('defense'),
				'speed' => $this->request->getPost('speed'),
				'special' => $this->request->getPost('special'),
				'picture' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/' . $this->request->getPost('numero') . '.png',
				'slug'  => url_title($this->request->getPost('name'), '-', TRUE),
				'type_1' => $type_1,
				'type_2' => $type_2,
			]);
			echo view('pokemon/success');
		} else {
			echo view('templates/header', ['title' => 'Error encountered, please try again']);
			echo view('pokemon/create', $data);
			echo view('templates/footer');
		}
	}

	public function edit($slug)
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();
		$data = [
			'title' => 'Pokedex',
			'pokemon'  => $pokemonModel->getPokemons($slug),
			'typeOne' => $typeModel->getTypeOneName($slug),
			'typeTwo' => $typeModel->getTypeTwoName($slug),
			'types' => $typeModel->getTypes(),
		];
		echo view('templates/header');
		echo view('pokemon/edit', $data);
		echo view('templates/footer');
	}

	public function update($slug)
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();
		$data = [
			'title' => 'Pokedex',
			'pokemon'  => $pokemonModel->getPokemons($slug),
			'slug'  => url_title($this->request->getPost('name'), '-', TRUE),
			'types' => $typeModel->getTypes(),

		];

		if ($this->request->getMethod() === 'post' && $this->validate([
			'name' => 'required|min_length[3]|max_length[255]',
			'numero' => 'required',
			'hp' => 'required',
			'attack' => 'required',
			'defense' => 'required',
			'speed' => 'required',
			'special' => 'required',
			'picture' => 'required',
			'type_1' => 'required',
			'type_2' => 'required',
		])) {
			$type_1 = $this->request->getPost('type_1');
			$type_2 = $this->request->getPost('type_2');
			if ($type_1 == $type_2) {
				// Which is 'no type found' to avoid dual type and BDD error
				$type_2 = 20;
			} else if ($type_1 == $type_2 && $type_1 == 20) {
				$type_2 = null;
			}
			$pokemonModel->update($data['pokemon']['id_pokemon'], [
				'numero' => $this->request->getPost('numero'),
				'name' => $this->request->getPost('name'),
				'hp' => $this->request->getPost('hp'),
				'attack' => $this->request->getPost('attack'),
				'defense' => $this->request->getPost('defense'),
				'speed' => $this->request->getPost('speed'),
				'special' => $this->request->getPost('special'),
				'picture' => 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/' . $this->request->getPost('numero') . '.png',
				'slug'  => url_title($this->request->getPost('name'), '-', TRUE),
				'type_1' => $type_1,
				'type_2' => $type_2,
			]);
			echo view('pokemon/success', $data);
		} else {
			echo view('templates/header', $data);
			echo view('pokemon/edit', $data);
			echo view('templates/footer', $data);
		}
	}

	public function remove($slug)
	{
		$pokemonModel = new PokemonModel();
		$typeModel = new TypeModel();
		// $data = [
		// 	'title' => 'Pokedex',
		// 	'pokemon'  => $pokemonModel->getPokemons($slug),
		// 	'slug'  => url_title($this->request->getPost('name'), '-', TRUE),
		// 	'types' => $typeModel->getTypes(),

		// ];
		$pokemonModel->removePokemon($slug);
		return redirect()->to('/pokemon');
	}
}
