<!doctype html>
<html>

<head>
    <title>CodeIgniter Tutorial</title>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <style>
        .navi {
            background: #029A81;
            padding: 20px 40px;
            font-size: 28px;
            text-align: center;
        }

        .navi span {
            margin: 5px 10px 5px 10px;
        }

        .navi span a {
            color: white;
            text-transform: uppercase;
            text-decoration: none;
            font-family: 'Oswald', sans-serif;
        }
    </style>

</head>

<body>

    <div class="navi">
        <span><a href="/">Home</a></span>

        <span><a href="/pokemon/create">Create</a></span>
    </div>