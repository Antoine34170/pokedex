<style>
    .striped tr {
        border-bottom: none
    }

    table.striped>tbody>tr:nth-child(odd) {
        background-color: rgba(242, 242, 242, 0.5)
    }

    table.striped>tbody>tr>td {
        border-radius: 0
    }

    table.highlight>tbody>tr {
        -webkit-transition: background-color .25s ease;
        transition: background-color .25s ease
    }

    table.highlight>tbody>tr:hover {
        background-color: rgba(242, 242, 242, 0.5)
    }

    table.centered thead tr th,
    table.centered tbody tr td {
        text-align: center
    }

    tr {
        border-bottom: 1px solid rgba(0, 0, 0, 0.12)
    }

    td,
    th {
        padding: 15px 5px;
        display: table-cell;
        text-align: left;
        vertical-align: middle;
        border-radius: 2px
    }

    @media only screen and (max-width: 992px) {
        table.responsive-table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            display: block;
            position: relative
        }

        table.responsive-table td:empty:before {
            content: '\00a0'
        }

        table.responsive-table th,
        table.responsive-table td {
            margin: 0;
            vertical-align: top
        }

        table.responsive-table th {
            text-align: left
        }

        table.responsive-table thead {
            display: block;
            float: left
        }

        table.responsive-table thead tr {
            display: block;
            padding: 0 10px 0 0
        }

        table.responsive-table thead tr th::before {
            content: "\00a0"
        }

        table.responsive-table tbody {
            display: block;
            width: auto;
            position: relative;
            overflow-x: auto;
            white-space: nowrap
        }

        table.responsive-table tbody tr {
            display: inline-block;
            vertical-align: top
        }

        table.responsive-table th {
            display: block;
            text-align: right
        }

        table.responsive-table td {
            display: block;
            min-height: 1.25em;
            text-align: left
        }

        table.responsive-table tr {
            border-bottom: none;
            padding: 0 10px
        }

        table.responsive-table thead {
            border: 0;
            border-right: 1px solid rgba(0, 0, 0, 0.12)
        }
    }

    .hoverable {
        -webkit-transition: -webkit-box-shadow .25s;
        transition: -webkit-box-shadow .25s;
        transition: box-shadow .25s;
        transition: box-shadow .25s, -webkit-box-shadow .25s
    }

    .hoverable:hover {
        -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
    }
</style>


<!-- Pokemon returned -->
<div class="container">


</div>

<div class="row">
    <div class="d-flex justify-content-center">
        <div class="col-12 col-lg-8">


            <div class="card hoverable my-5">
                <div class="card-header">

                    <h3 class="text-center"> <?= esc($pokemon['name']) ?></h3>
                </div>

                <div class="d-flex justify-content-center">
                    <img src="<?= esc($pokemon['picture']) ?>">
                </div>

                <div class="card-stacked">
                    <div class="card-content">
                        <table class="bordered striped w-100">
                            <tbody>
                                <tr>
                                    <td>Nom</td>
                                    <td><strong><?= esc($pokemon['name']) ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Points de vie</td>
                                    <td><strong><?= esc($pokemon['hp']) ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Dégâts</td>
                                    <td><strong><?= esc($pokemon['attack']) ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Defense</td>
                                    <td><strong><?= esc($pokemon['speed']) ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Vitesse</td>
                                    <td><strong><?= esc($pokemon['speed']) ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Special</td>
                                    <td><strong><?= esc($pokemon['special']) ?></strong></td>
                                </tr>

                                <!-- ---------- -->
                                <?php if (is_null(esc($typeOne))) : ?>


                                <?php else : ?>

                                    <!-- Type One -->
                                    <?php if (esc($typeOne[0]['id_type']) == '20') : ?>
                                    <?php else : ?>
                                        <tr>
                                            <td>Type 1</td>
                                            <td><strong><?= esc($typeOne[0]['name_type']) ?></strong></td>
                                        </tr>
                                    <?php endif ?>
                                    <!-- Type Two -->
                                    <?php if (esc($typeTwo[0]['id_type']) == '20') : ?>
                                    <?php else : ?>
                                        <tr>
                                            <td>Type 2</td>
                                            <td><strong><?= esc($typeTwo[0]['name_type']) ?></strong></td>
                                        </tr>
                                    <?php endif ?>

                                <?php endif ?>








                            </tbody>
                        </table>
                    </div>
                    <div class="card-action d-flex justify-content-center mt-3">
                        <a href="/pokemon" class="hoverable waves-effect wave-light btn"><strong>Back</strong></a>
                        <a href="/pokemon/edit/<?= esc($pokemon['slug']) ?>" class="hoverable waves-effect wave-light btn">Edit</a>

                        <button class="hoverable waves-effect wave-light btn" data-bs-toggle="modal" data-bs-target="#removalModal">Remove</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="removalModal" tabindex="-1" aria-labelledby="removalModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="removalModalLabel">Are you sure ?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Do you really want to remove this sweety pokemon ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Nooooo...</button>
                <form action="/pokemon/remove/<?= esc($pokemon['id_pokemon']) ?>" method="post">
                    <button class="waves-effect wave-light btn" type="submit" name="submit" class="btn btn-primary">Sure</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- <h4 *ngIf='!pokemon' class='center'>
    <pkmn-loader></pkmn-loader>>
</h4> -->