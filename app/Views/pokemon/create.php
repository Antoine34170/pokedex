<h2 class="text-center my-5"><?= esc($title) ?></h2>

<?= \Config\Services::validation()->listErrors() ?>

<div class="container">


    <form action="/pokemon/create" method="post">
        <?= csrf_field() ?>


        <div class="mb-3">
            <label for="numero" class="form-label">Numero pokemon</label>
            <input type="text" class="form-control" id="numero" name="numero">
        </div>


        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="mb-3">
            <label for="hp" class="form-label">Hp</label>
            <input type="text" class="form-control" id="hp" name="hp">
        </div>

        <div class="mb-3">
            <label for="attack" class="form-label">Attack</label>
            <input type="text" class="form-control" id="attack" name="attack">
        </div>

        <div class="mb-3">
            <label for="defense" class="form-label">Defense</label>
            <input type="text" class="form-control" id="defense" name="defense">
        </div>

        <div class="mb-3">
            <label for="speed" class="form-label">Speed</label>
            <input type="text" class="form-control" id="speed" name="speed">
        </div>

        <div class="mb-3">
            <label for="special" class="form-label">Special</label>
            <input type="text" class="form-control" id="special" name="special">
        </div>

        <div class=" mb-3 d-none">
            <label for="picture" class="form-label">Picture</label>
            <input type="text" class="form-control" id="picture" name="picture">
        </div>

        <tr>
            <td>Type 1</td>
            <td>
                <select name="type_1" id="type_1">
                    <?php foreach ($types as $types_item) : ?>
                        <option value="<?= esc($types_item['id_type']) ?>"><?= esc($types_item['name_type']) ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>Type 2</td>
            <td>
                <select name="type_2" id="type_2">
                    <?php foreach ($types as $types_item) : ?>
                        <option value="<?= esc($types_item['id_type']) ?>"><?= esc($types_item['name_type']) ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>

        <!-- <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="mb-3">
            <label for="hp" class="form-label">Hp</label>
            <input type="text" class="form-control" id="hp" name="hp">
        </div>

        <div class="mb-3">
            <label for="attack" class="form-label">Attack</label>
            <input type="text" class="form-control" id="attack" name="attack">
        </div>

        <div class="mb-3">
            <label for="defense" class="form-label">Defense</label>
            <input type="text" class="form-control" id="defense" name="defense">
        </div>

        <div class="mb-3">
            <label for="speed" class="form-label">Speed</label>
            <input type="text" class="form-control" id="speed" name="speed">
        </div>

        <div class="mb-3">
            <label for="special" class="form-label">Special</label>
            <input type="text" class="form-control" id="special" name="special">
        </div>

        <div class="mb-3">
            <label for="picture" class="form-label">Picture</label>
            <input type="text" class="form-control" id="picture" name="picture">
        </div> -->


        <input type="submit" name="submit" value="Create pokemon item" />
    </form>
</div>