<h2 class="text-center my-5"><?= esc($title) ?></h2>

<?php if (!empty($pokemon) && is_array($pokemon)) : ?>
    <!-- Pokemon returned -->
    <div class="container">
        <div class="row">
            <!-- Loop on every Pokemon to build cards  -->
            <?php foreach ($pokemon as $pokemon_item) : ?>
                <div class="col-12 col-lg-3 mb-5">
                    <div class="card horizontal border border-black mx-3" id=<?= esc($pokemon_item['name']) ?>>

                        <!-- checking if Type_1 of Pokemon exist on pokemon_type table -->
                        <?php foreach ($types as $types_item) : ?>
                            <?php if (esc($pokemon_item['type_1']) == esc($types_item['id_type'])) : ?>
                                <script>
                                    $("#<?= esc($pokemon_item['name']) ?>").css("background", "linear-gradient(0.35turn,#<?= esc($types_item['color_type']) ?>, #FFFFFF)")
                                </script>
                                <?php foreach ($types as $types_items) : ?>
                                    <?php if (esc($pokemon_item['type_2']) == esc($types_items['id_type'])) : ?>

                                        <script>
                                            $("#<?= esc($pokemon_item['name']) ?>").css("background", "linear-gradient(0.35turn, #<?= esc($types_item['color_type']) ?>, #<?= esc($types_items['color_type']) ?>)")
                                        </script>
                                    <?php else : ?>
                                    <?php endif ?>
                                <?php endforeach; ?>



                            <?php else : ?>

                            <?php endif ?>

                        <?php endforeach; ?>

                        <div class="card-stacked">
                            <div class="card-content">
                                <h4 class="text-center">#<?= esc($pokemon_item['numero']) ?> <?= esc($pokemon_item['name']) ?></h4>
                            </div>
                        </div>
                        <div class="card-image d-flex justify-content-center">
                            <img src=<?= esc($pokemon_item['picture']) ?>>
                        </div>
                        <p class="text-center"><a href="/pokemon/view/<?= esc($pokemon_item['slug'], 'url') ?>"><?= esc($pokemon_item['name']) ?> is my favorite</a></p>
                    </div>

                    <!-- <?= esc($pokemon_item['name']) ?> -->


                </div>




            <?php endforeach; ?>

            <!-- End Pokemon returned -->

        </div>
    </div>

    <!-- No pokemon found -->
<?php else : ?>

    <h3>No pokemon</h3>

    <p>Unable to find any pokemon for you.</p>

<?php endif ?>
<!-- End no pokemon found -->