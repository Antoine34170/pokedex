<style>
    .striped tr {
        border-bottom: none
    }

    table.striped>tbody>tr:nth-child(odd) {
        background-color: rgba(242, 242, 242, 0.5)
    }

    table.striped>tbody>tr>td {
        border-radius: 0
    }

    table.highlight>tbody>tr {
        -webkit-transition: background-color .25s ease;
        transition: background-color .25s ease
    }

    table.highlight>tbody>tr:hover {
        background-color: rgba(242, 242, 242, 0.5)
    }

    table.centered thead tr th,
    table.centered tbody tr td {
        text-align: center
    }

    tr {
        border-bottom: 1px solid rgba(0, 0, 0, 0.12)
    }

    td,
    th {
        padding: 15px 5px;
        display: table-cell;
        text-align: left;
        vertical-align: middle;
        border-radius: 2px
    }

    @media only screen and (max-width: 992px) {
        table.responsive-table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            display: block;
            position: relative
        }

        table.responsive-table td:empty:before {
            content: '\00a0'
        }

        table.responsive-table th,
        table.responsive-table td {
            margin: 0;
            vertical-align: top
        }

        table.responsive-table th {
            text-align: left
        }

        table.responsive-table thead {
            display: block;
            float: left
        }

        table.responsive-table thead tr {
            display: block;
            padding: 0 10px 0 0
        }

        table.responsive-table thead tr th::before {
            content: "\00a0"
        }

        table.responsive-table tbody {
            display: block;
            width: auto;
            position: relative;
            overflow-x: auto;
            white-space: nowrap
        }

        table.responsive-table tbody tr {
            display: inline-block;
            vertical-align: top
        }

        table.responsive-table th {
            display: block;
            text-align: right
        }

        table.responsive-table td {
            display: block;
            min-height: 1.25em;
            text-align: left
        }

        table.responsive-table tr {
            border-bottom: none;
            padding: 0 10px
        }

        table.responsive-table thead {
            border: 0;
            border-right: 1px solid rgba(0, 0, 0, 0.12)
        }
    }

    .hoverable {
        -webkit-transition: -webkit-box-shadow .25s;
        transition: -webkit-box-shadow .25s;
        transition: box-shadow .25s;
        transition: box-shadow .25s, -webkit-box-shadow .25s
    }

    .hoverable:hover {
        -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
    }
</style>

<!-- Pokemon returned -->
<div class="container">
    <form action="/pokemon/update/<?= esc($pokemon['slug']) ?>" method="post">
        <?= csrf_field() ?>
        <h1></h1>



        <div class="row">
            <div class="d-flex justify-content-center">
                <div class="col-12 col-lg-8">


                    <div class="card hoverable my-5">
                        <div class="card-header">
                            <h3 class="text-center"> <?= esc($pokemon['name']) ?></h3>
                        </div>


                        <div class="d-flex justify-content-center">
                            <img src="<?= esc($pokemon['picture']) ?>">
                        </div>

                        <div class="card-stacked">
                            <div class="card-content">
                                <table class="bordered striped w-100">
                                    <tbody>
                                        <div class="d-none">
                                            <tr class="d-none">
                                                <td class="d-none">id</td>
                                                <td class="d-none"><strong><input type="text" value="<?= esc($pokemon['id_pokemon']) ?>"></strong></td>
                                            </tr>
                                        </div>
                                        <div class="mb-3">
                                            <label for="numero" class="form-label">Numero pokemon</label>
                                            <input type="text" class="form-control" id="numero" name="numero" value="<?= esc($pokemon['numero']) ?>">
                                        </div>


                                        <div class="mb-3">
                                            <label for="name" class="form-label">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" value="<?= esc($pokemon['name']) ?>">
                                        </div>

                                        <div class="mb-3">
                                            <label for="hp" class="form-label">Hp</label>
                                            <input type="text" class="form-control" id="hp" name="hp" value="<?= esc($pokemon['hp']) ?>">
                                        </div>

                                        <div class="mb-3">
                                            <label for="attack" class="form-label">Attack</label>
                                            <input type="text" class="form-control" id="attack" name="attack" value="<?= esc($pokemon['attack']) ?>">
                                        </div>

                                        <div class="mb-3">
                                            <label for="defense" class="form-label">Defense</label>
                                            <input type="text" class="form-control" id="defense" name="defense" value="<?= esc($pokemon['defense']) ?>">
                                        </div>

                                        <div class="mb-3">
                                            <label for="speed" class="form-label">Speed</label>
                                            <input type="text" class="form-control" id="speed" name="speed" value="<?= esc($pokemon['speed']) ?>">
                                        </div>

                                        <div class="mb-3">
                                            <label for="special" class="form-label">Special</label>
                                            <input type="text" class="form-control" id="special" name="special" value="<?= esc($pokemon['special']) ?>">
                                        </div>

                                        <div class=" mb-3 d-none">
                                            <label for="picture" class="form-label">Picture</label>
                                            <input type="text" class="form-control" id="picture" name="picture" value="<?= esc($pokemon['numero']) ?>">
                                        </div>

                                        <tr>
                                            <td>Type 1</td>
                                            <td>
                                                <select name="type_1" id="type_1">
                                                    <?php foreach ($types as $types_item) : ?>
                                                        <?php if (esc($typeOne[0]['id_type']) == esc($types_item['id_type'])) : ?>
                                                            <option value="<?= esc($types_item['id_type']) ?>" selected=selected><?= esc($types_item['name_type']) ?></option>
                                                        <?php else : ?>
                                                            <option value="<?= esc($types_item['id_type']) ?>"><?= esc($types_item['name_type']) ?></option>
                                                        <?php endif ?>

                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Type 2</td>
                                            <td>
                                                <select name="type_2" id="type_2">
                                                    <?php foreach ($types as $types_item) : ?>
                                                        <?php if (esc($typeTwo[0]['id_type']) == esc($types_item['id_type'])) : ?>
                                                            <option value="<?= esc($types_item['id_type']) ?>" selected=selected><?= esc($types_item['name_type']) ?></option>
                                                        <?php else : ?>
                                                            <option value="<?= esc($types_item['id_type']) ?>"><?= esc($types_item['name_type']) ?></option>
                                                        <?php endif ?>

                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-action">
                                <a href="/pokemon" class="waves-effect wave-light btn">Retour</a>
                                <form action="/pokemon/remove/<?= esc($pokemon['slug']) ?>" method="post">
                                    <button class="waves-effect wave-light btn" type="submit" name="submit">Update this pokemon </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>