-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 15 juil. 2021 à 06:56
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9
SET
  SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

START TRANSACTION;

SET
  time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;

/*!40101 SET NAMES utf8mb4 */
;

--
-- Base de données : `pokemon`
--
-- --------------------------------------------------------
--
-- Structure de la table `pokemon`
--
DROP TABLE IF EXISTS `pokemon`;

CREATE TABLE IF NOT EXISTS `pokemon` (
  `id_pokemon` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `hp` int NOT NULL,
  `attack` int NOT NULL,
  `defense` int NOT NULL,
  `speed` int NOT NULL,
  `special` int NOT NULL,
  `picture` varchar(80) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `type_1` int DEFAULT NULL,
  `type_2` int DEFAULT '20',
  PRIMARY KEY (`id_pokemon`),
  KEY `type_1_FK` (`type_1`),
  KEY `type_2` (`type_2`)
) ENGINE = InnoDB AUTO_INCREMENT = 31 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pokemon`
--
INSERT INTO
  `pokemon` (
    `id_pokemon`,
    `name`,
    `numero`,
    `hp`,
    `attack`,
    `defense`,
    `speed`,
    `special`,
    `picture`,
    `slug`,
    `type_1`,
    `type_2`
  )
VALUES
  (
    1,
    'Bulbasaurz',
    '001',
    46,
    49,
    49,
    45,
    66,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png',
    'bulbasaurz',
    6,
    20
  ),
  (
    2,
    'Ivysaur',
    '002',
    60,
    66,
    62,
    60,
    81,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png',
    'ivysaur',
    6,
    20
  ),
  (
    3,
    'Venusaur',
    '003',
    80,
    82,
    83,
    80,
    100,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/003.png',
    'venusaur',
    6,
    20
  ),
  (
    4,
    'Charmander',
    '004',
    39,
    52,
    43,
    65,
    50,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png',
    'charmander',
    19,
    20
  ),
  (
    5,
    'Charmeleon',
    '005',
    58,
    64,
    58,
    80,
    65,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/005.png',
    'charmeleon',
    19,
    20
  ),
  (
    6,
    'Charizard',
    '006',
    78,
    84,
    78,
    100,
    85,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
    'charizard',
    19,
    18
  ),
  (
    7,
    'Squirtle',
    '007',
    44,
    48,
    65,
    43,
    50,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png',
    'squirtle',
    11,
    20
  ),
  (
    8,
    'Wartortle',
    '008',
    59,
    63,
    80,
    58,
    65,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/008.png',
    'wartortle',
    11,
    20
  ),
  (
    9,
    'Blastoise',
    '009',
    79,
    83,
    100,
    78,
    85,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png',
    'blastoise',
    11,
    20
  ),
  (
    10,
    'Caterpie',
    '010',
    45,
    30,
    35,
    45,
    20,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/010.png',
    'caterpie',
    5,
    20
  ),
  (
    11,
    'Metapod',
    '011',
    50,
    20,
    55,
    30,
    25,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/011.png',
    'metapod',
    5,
    20
  ),
  (
    12,
    'Butterfree',
    '012',
    60,
    45,
    50,
    70,
    80,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/012.png',
    'butterfree',
    5,
    18
  ),
  (
    13,
    'Weedle',
    '013',
    40,
    35,
    30,
    50,
    20,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/013.png',
    'weedle',
    5,
    20
  ),
  (
    14,
    'Kakuna',
    '014',
    45,
    25,
    50,
    35,
    25,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/014.png',
    'kakuna',
    5,
    20
  ),
  (
    15,
    'Beedrill',
    '015',
    65,
    80,
    40,
    75,
    45,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/015.png',
    'beedrill',
    5,
    18
  ),
  (
    16,
    'Pidgey',
    '016',
    40,
    45,
    40,
    56,
    35,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png',
    'pidgey',
    18,
    20
  ),
  (
    17,
    'Pidgeotto',
    '017',
    63,
    60,
    55,
    71,
    50,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/017.png',
    'pidgeotto',
    18,
    20
  ),
  (
    18,
    'Pidgeot',
    '018',
    83,
    80,
    75,
    91,
    70,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/018.png',
    'pidgeot',
    18,
    NULL
  ),
  (
    19,
    'Rattata',
    '019',
    30,
    56,
    35,
    72,
    25,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/019.png',
    'rattata',
    14,
    NULL
  ),
  (
    20,
    'Raticate',
    '020',
    55,
    81,
    60,
    97,
    50,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/020.png',
    'raticate',
    14,
    NULL
  ),
  (
    21,
    'Spearow',
    '021',
    40,
    60,
    30,
    70,
    31,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/021.png',
    'spearow',
    18,
    NULL
  ),
  (
    22,
    'Fearow',
    '022',
    65,
    90,
    65,
    100,
    61,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/022.png',
    'fearow',
    18,
    NULL
  ),
  (
    23,
    'Ekans',
    '023',
    35,
    60,
    44,
    55,
    40,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/023.png',
    'ekans',
    15,
    NULL
  ),
  (
    24,
    'Arbok',
    '024',
    60,
    85,
    69,
    80,
    65,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/024.png',
    'arbok',
    15,
    NULL
  ),
  (
    25,
    'Pikachu',
    '025',
    35,
    55,
    30,
    90,
    50,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png',
    'pikachu',
    NULL,
    20
  ),
  (
    26,
    'Raichu',
    '026',
    60,
    90,
    55,
    100,
    90,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/026.png',
    'raichu',
    3,
    NULL
  ),
  (
    27,
    'Sandshrew',
    '027',
    50,
    75,
    85,
    40,
    30,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/027.png',
    'sandshrew',
    8,
    NULL
  ),
  (
    28,
    'Sandslash',
    '028',
    75,
    100,
    110,
    65,
    55,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/028.png',
    'sandslash',
    8,
    NULL
  ),
  (
    29,
    'Nidoran',
    '029',
    50,
    45,
    50,
    50,
    45,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/029.png',
    'nidoran',
    14,
    NULL
  ),
  (
    30,
    'Nidorino',
    '030',
    50,
    45,
    85,
    74,
    62,
    'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/030.png',
    'nidorino',
    14,
    NULL
  );

-- --------------------------------------------------------
--
-- Structure de la table `pokemon_type`
--
DROP TABLE IF EXISTS `pokemon_type`;

CREATE TABLE IF NOT EXISTS `pokemon_type` (
  `id_type` int NOT NULL AUTO_INCREMENT,
  `name_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE = InnoDB AUTO_INCREMENT = 21 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pokemon_type`
--
INSERT INTO
  `pokemon_type` (`id_type`, `name_type`)
VALUES
  (1, 'acier'),
  (2, 'dragon'),
  (3, 'electrique'),
  (5, 'insecte'),
  (6, 'plante'),
  (7, 'psy'),
  (8, 'sol'),
  (9, 'ténèbres'),
  (10, 'combat'),
  (11, 'eau'),
  (12, 'fée'),
  (13, 'glace'),
  (14, 'normal'),
  (15, 'poison'),
  (16, 'roche'),
  (17, 'spectre'),
  (18, 'vol'),
  (19, 'feu'),
  (20, 'not found');

--
-- Contraintes pour les tables déchargées
--
--
-- Contraintes pour la table `pokemon`
--
ALTER TABLE
  `pokemon`
ADD
  CONSTRAINT `TYPE_1_FK` FOREIGN KEY (`type_1`) REFERENCES `pokemon_type` (`id_type`),
ADD
  CONSTRAINT `TYPE_2_FK` FOREIGN KEY (`type_2`) REFERENCES `pokemon_type` (`id_type`);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;