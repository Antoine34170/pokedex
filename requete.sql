$builder = $this->db->table('pokemon');
        $builder->select('name');
        $builder->join('pokemon__pokemon_type', 'pokemon__pokemon_type.id_pokemon = pokemon.id');
        $builder->join('pokemon_type', 'pokemon_type.id = pokemon__pokemon_type.id_pokemon_type');
        $builder->where(['pokemon.slug' => $slug]);
        $result = $builder->get()->getResult();

        return json_decode(json_encode($result), true);